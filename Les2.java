public class Les2 {
     public static void main(String[] args) {
		Volvo myCar = new Volvo();
		myCar.pathInMiles = 80;
		myCar.Move();
     }
}

class Car implements Movable {
	public int pathInMiles; 
	public void Move() {
		System.out.println("You driven above " + pathInMiles + " miles");
	}
}

interface Movable {
	public void Move();

}

class Volvo extends Car {
	
}

